---
title: Overview
type: Overview
---

[Nats](https://nats.io/) is a high performance messaging system that acts as a distributed messaging queue for cloud native applications.
