---
title: Overview
type: Overview
---

[Kibana](https://elastic.co/) te permite visualizar tus datos de Elasticsearch y navegar por el Elastic Stack para que puedas hacer cualquier cosa, desde rastrear la carga de búsqueda hasta comprender cómo fluyen las solicitudes a través de tus aplicaciones.
