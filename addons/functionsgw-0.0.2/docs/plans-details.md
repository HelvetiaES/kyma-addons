---
title: Services and Plans
type: Details
---

## Service description

The Function GW Service Class provides the following plans:

| Plan Name | Description |
|-----------|-------------|
| `enterprise` | Agent enterprise plan which uses Redis as persistence. |
| `micro` | Agent micro plan which uses the in-memory persistence. |


## Provision

This service provisions a new Function GW.

### Provisioning parameters

These are the provisioning parameters:

| Parameter Name | Type | Description | Required | Default Value |
|----------------|------|-------------|----------|---------------|
| **imagePullPolicy** | `string` | Specifies how the kubelet pulls images from the specified registry. The possible values are `Always`, `IfNotPresent`, `Never`. | NO | `IfNotPresent` |
