---
title: Services and Plans
type: Details
---

## Service description

The Agent Service Class provides the following plans:

| Plan Name | Description |
|-----------|-------------|
| `enterprise` | Agent enterprise plan which uses Redis as persistence. |
| `micro` | Agent micro plan which uses the in-memory persistence. |


## Provision

This service provisions a new Agent.

### Provisioning parameters

These are the provisioning parameters:

| Parameter Name | Type | Description | Required | Default Value |
|----------------|------|-------------|----------|---------------|
| **agentName** | `string` | Specifies Agent Name | YES | |
| **apiKey** | `string` | Specifies Watson Api Key | YES | |
| **assistantId** | `string` | Specifies Watson Assistant ID | YES | |
| **gateway** | `string` | Specifies the gateway (part of URL). The possible values are `gateway-fra`. | YES | `gateway-fra` |
| **elasticHost** | `string` | Specifies the Elastic Search Host | YES | |
| **imagePullPolicy** | `string` | Specifies how the kubelet pulls images from the specified registry. The possible values are `Always`, `IfNotPresent`, `Never`. | NO | `IfNotPresent` |
