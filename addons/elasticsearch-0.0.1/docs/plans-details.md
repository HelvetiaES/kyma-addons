---
title: Services and Plans
type: Details
---

## Service description

The Elasticsearch Service Class provides the following plans:

| Plan Name | Description |
|-----------|-------------|
| `enterprise` | Elasticsearch enterprise plan which uses a cluster of 3 masters. |
| `micro` | Elasticsearch micro plan which uses only one master. |


## Provision

This service provisions a new Elasticsearch cluster.

### Provisioning parameters

These are the provisioning parameters:

| Parameter Name | Type | Description | Required | Default Value |
|----------------|------|-------------|----------|---------------|
| **imagePullPolicy** | `string` | Specifies how the kubelet pulls images from the specified registry. The possible values are `Always`, `IfNotPresent`, `Never`. | NO | `IfNotPresent` |

## Credentials

The binding creates a Secret with the following credentials:

| Parameter Name | Type | Description |
|----------------|------|-------------|
| **HOST** | `string` | The fully-qualified address of the Elasticsearch. |
| **PORT** | `int` | The port number to connect to the Elasticsearch. |
| **ELASTICSEARCH_PASSWORD** | `string` | The password to the Elasticsearch. |
